package com.example.service;

import com.example.model.Car;
import com.example.model.CarType;
import com.example.store.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CarService {

    private final CarRepository carRepository;

    @Autowired
    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public Car createCar(Car car) throws Exception {
        Optional<Car> carStoredOpt = carRepository.save(car);

        if (!carStoredOpt.isPresent()) {
            throw new Exception("Error");
        }

        if (carStoredOpt.get().getNumOfSeats() == 4) {
            car.setType(CarType.BERLIN);
        }
        return car;
    }

    public Car getCar(String id) throws Exception {
        Optional<Car> carStoredOpt = carRepository.find(id);

        if (!carStoredOpt.isPresent()) {
            throw new Exception("Error");
        }

        return carStoredOpt.get();
    }
}
