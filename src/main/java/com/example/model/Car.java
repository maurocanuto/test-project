package com.example.model;

public class Car {
    private String id;
    private Integer numOfSeats;
    private String name;
    private CarType type;

    public Car(String id, Integer numOfSeats, String name) {
        this.id = id;
        this.numOfSeats = numOfSeats;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public Integer getNumOfSeats() {
        return numOfSeats;
    }

    public String getName() {
        return name;
    }

    public CarType getType() {
        return type;
    }

    public void setType(CarType type) {
        this.type = type;
    }
}
