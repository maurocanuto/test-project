package com.example.rest.mapper;

import com.example.model.Car;
import com.example.rest.dto.CarDTO;

import java.util.UUID;

public class CarMapper {
    public static Car makeCar(CarDTO carDTO) {
        if (carDTO.getNumOfSeats() < 0) {
            return new Car(UUID.randomUUID().toString(), 4, carDTO.getName());
        }
        return new Car(UUID.randomUUID().toString(), carDTO.getNumOfSeats(), carDTO.getName());
    }

    public static CarDTO makeCareDTO(Car car) {
        return new CarDTO(car.getId(), car.getNumOfSeats(), car.getName());
    }
}
