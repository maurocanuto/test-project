package com.example.rest;

import com.example.model.Car;
import com.example.rest.dto.CarDTO;
import com.example.rest.mapper.CarMapper;
import com.example.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cars")
public class CarController {

    private final CarService carService;

    @Autowired
    public CarController(CarService carService) {
        this.carService = carService;
    }

    @PostMapping
    public CarDTO createCar(@Valid @RequestBody CarDTO carDTO) throws Exception {
        Car car = CarMapper.makeCar(carDTO);
        return CarMapper.makeCareDTO(carService.createCar(car));
    }

    @GetMapping("/{id}")
    public CarDTO getCar(@PathVariable String id) throws Exception {
        return CarMapper.makeCareDTO(carService.getCar(id));
    }
}
