package com.example.rest.dto;

public class CarDTO {
    private String id;
    private Integer numOfSeats;
    private String name;

    public CarDTO() {
    }

    public CarDTO(String id, Integer numOfSeats, String name) {
        this.id = id;
        this.numOfSeats = numOfSeats;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public Integer getNumOfSeats() {
        return numOfSeats;
    }

    public String getName() {
        return name;
    }
}
