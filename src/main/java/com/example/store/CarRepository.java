package com.example.store;

import com.example.model.Car;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Optional;

@Repository
public class CarRepository {
    private HashMap<String, Car> db = new HashMap<>();

    public Optional<Car> save(Car car) {
        db.put(car.getId(), car);
        return Optional.of(car);
    }

    public Optional<Car> find(String id) {
        Car result = db.get(id);
        if (result == null) {
            return Optional.empty();
        }
        return Optional.of(result);
    }
}
